# Monterail recruitment tasks (1,2,3)

> Static HTML - way.

## Build Setup

``` bash
# Node.js environment setup
node setup

# (Make sure npm config production value is set to false, if not, run):
# npm config set -g production false

# install dependencies
npm install

# serve with browsersync at localhost:3000
gulp

# build for production with minification
gulp build
```