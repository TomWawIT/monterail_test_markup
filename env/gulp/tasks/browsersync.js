/*
 |--------------------------------------------------------------------------
 | Browsersync
 |--------------------------------------------------------------------------
 |
 | Browsersync is a tool that provides synchronized testing environment.
 | When navigating website on one device all user actions are automatically
 | replicated on all other devices where the website is open.
 | Browsersync also provides a live reload.
 |
 | More information:
 | https://www.browsersync.io/
 |
 */

let browserSync     = require('browser-sync');
let getProjectUrl   = require('../../lib/getProjectUrl');
let messageFromFile = require('../../lib/messageFromFile');
let config          = require('../../lib/getProjectConfig');

module.exports = () => {
  if ('markup' === config('project.type')) {
    browserSync({
      server: config('paths.public'),
      files: `${config('paths.public')}/**/*.*`
    });

    return;
  }

  let projectUrl = getProjectUrl();
  if (!projectUrl) {
    messageFromFile('env/messages/project-url-missing');
    return;
  }

  browserSync.init({
    proxy: projectUrl,
    files: `${config('paths.public')}/**/*.*`
  });
};
