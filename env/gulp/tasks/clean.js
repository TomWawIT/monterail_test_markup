/*
|--------------------------------------------------------------------------
| Clean
|--------------------------------------------------------------------------
|
| Clears the contents of the public directory.
| Used at the beginning of the build to prepare for a clean build.
|
*/

let del    = require('del');
let config = require('../../lib/getProjectConfig');

module.exports = () => {
  return del([
    `${config('paths.public')}/**/*`,

    /*
     * In WordPress projects we don't want to delete uploads every time
     * we build the project, or we'll have to keep regenerating thumbnails.
     */
    `!${config('paths.public')}/app`,
    `!${config('paths.public')}/app/uploads`,
    `!${config('paths.public')}/app/uploads/**/*`
  ]);
};
