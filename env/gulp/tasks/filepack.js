let fs           = require('fs-extra');
let Archive      = require('../../lib/Archive');
let isProduction = require('../../lib/isProduction');
let config       = require('../../lib/getProjectConfig');
let dump         = require('../../lib/dbDump');
let fileExists   = require('../../lib/fileExists');

let packers = {

  /**
   * Markup project filepack
   */
  markup: () => {
    let archive = new Archive(config('paths.public'), 'filepack.zip');

    // Public folder contents
    archive.archive.glob('**/*', {
      cwd: config('paths.public'),
      ignore: 'filepack.zip'
    });

    // Sources, if configured to include sources
    if (true === config('project.sources')) {
      archive.add(['**/*.{scss,sass}'], config('paths.source'), 'sources/styles');
    }

    if (true === config('project.sources') || config('components.webpack')) {
      archive.add(['**/*.{js,jsx}'], config('paths.source'), 'sources/js');
    }

    archive.save();
  },

  /**
   * WordPress project filepack
   */
  wordpress: () => {
    let archive = new Archive(config('paths.public'), 'filepack.zip');

    // Themes
    let themes = fs.readdirSync(`${config('paths.public')}/app/themes`);
    themes.forEach(theme => {
      archive.archive.directory(
        `${config('paths.public')}/app/themes/${theme}`,
        `themes/${theme}`
      );
    });

    // Plugins
    if (fileExists(`${config('paths.public')}/app/plugins`)) {
      archive.archive.directory(
        `${config('paths.public')}/app/plugins`,
        'plugins'
      );
    }

    // Uploads
    if (fileExists(`${config('paths.source')}/app/uploads`)) {
      archive.archive.directory(
        `${config('paths.source')}/app/uploads`,
        'uploads'
      );
    }

    // Installation instructions
    if (fileExists('resources/How-to-install.txt')) {
      archive.archive.file(
        'resources/How-to-install.txt',
        {name: 'How-to-install.txt'}
      );
    }

    // Sources, if configured to include sources
    themes.forEach(theme => {
      if (true === config('project.sources')) {
        archive.add(
          [ '**/*.{scss,sass}' ],
          `${config('paths.source')}/app/themes/${theme}`,
          `sources/${theme}/styles`
        );
      }

      if (true === config('project.sources') || config('components.webpack')) {
        archive.add(
          [ '**/*.{js,jsx}' ],
          `${config('paths.source')}/app/themes/${theme}`,
          `sources/${theme}/js`
        );
      }
    });

    // Database dump
    dump('db');
    archive.archive.file('resources/dumps/db.sql.tpl', { name: 'db.sql.tpl' });

    archive.save();
  }
};

module.exports = {
  default: () => {
    let packer = packers[ config('project.type') ];
    if (packer) {
      packer();
    }
  }
};
