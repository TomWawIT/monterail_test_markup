/*
|--------------------------------------------------------------------------
| Styles
|--------------------------------------------------------------------------
|
| Contains tasks handling styles.
| The default task compiles styles sources (*.sass, *.scss) from source
| to the pubic directory and validates them with csslint. The styles are
| processed using autoprefixer and on production are also automatically
| minified.
|
*/

let gulp         = require('gulp');
let path         = require('path');
let $            = require('gulp-load-plugins')({ lazy: true });
let autoprefixer = require('autoprefixer');
let cssnano      = require('cssnano');
let config       = require('../../lib/getProjectConfig');
let isProduction = require('../../lib/isProduction');
let fs           = require('fs');

module.exports = {
  /**
   * Compile style sources from source to public directory.
   */
  default: () => {
    // Add custom formatter for csslint displaying report in console
    $.csslint.addFormatter('csslint-stylish');

    let lintOutput = '';

    let task = gulp.src([
      `${config('paths.source')}/**/*.{sass,scss}`,
      `!${config('paths.source')}/**/__*{,/**}`
    ])
      .pipe($.plumber())

    if (true !== global.isWatching) {
      task.pipe($.changed(config('paths.public')))
    }

    task

      // Compile styles
      .pipe($.sass({
        outputStyle: 'expanded',
        precision: 8
      }))
      .on('error', $.sass.logError)

      // Group media queries
      .pipe($.groupCssMediaQueries())

      // Process compiled styles
      .pipe($.postcss([ autoprefixer ]))

      // Validate styles using csslint
      // Write the report to: reports/csslint
      .pipe($.csslint())
      .pipe($.csslint.formatter('stylish', {
        logger: str => {
          lintOutput += str;
        }
      }))
      .on('end', () => {
        fs.writeFile('reports/csslint', lintOutput)
      })

      // Output non-minified files
      .pipe(gulp.dest(config('paths.public')))

      // Minifiy files and output them with .min.css extension
      .pipe($.postcss([ cssnano ]))
      .pipe($.rename({ extname: '.min.css' }))
      .pipe(gulp.dest(config('paths.public')));

    return task;
  },
};
