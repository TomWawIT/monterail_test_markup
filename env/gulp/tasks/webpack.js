let gulp         = require('gulp');
let $            = require('gulp-load-plugins')({ lazy: true });
let webpack      = require('webpack');
let config       = require('../../lib/getProjectConfig');

function getWebpack(watch = false) {
  let webpackComponent = config('components.webpack');
  if (!webpackComponent) {
    return;
  }

  let settings = require('../../../webpack.config');
  let input = settings.entry;
  delete settings.context;
  delete settings.entry;

  if (true === watch) {
    settings.watch = true;
  }

  return gulp.src(`${config('paths.source')}/${input}`)
    .pipe($.plumber())
    .pipe($.webpack(settings))
    .pipe(gulp.dest(config('paths.public')));
}

module.exports = {
  default: () => {
    return getWebpack();
  },

  watch: () => {
    return getWebpack(true);
  }
};
