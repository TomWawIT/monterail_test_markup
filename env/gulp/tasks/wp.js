let gulp              = require('gulp');
let changed           = require('gulp-changed');
let del               = require('del');
let fs                = require('fs-extra');
let StringDecoder     = require('string_decoder').StringDecoder;
let replace           = require('replace');
let fileExists        = require('../../lib/fileExists');
let messageFromFile   = require('../../lib/messageFromFile');
let config            = require('../../lib/getProjectConfig');
let param             = require('../../lib/getParameter');
let wpcli             = require('../../lib/wpcli');
let serializedReplace = require('../../lib/replaceText');
let dump              = require('../../lib/dbDump');

module.exports = {

  /**
   * Install (copy from vendor) Bedrock.
   *
   * Copy only Bedrock files that has changed (in case of bedrock update)
   */
  [ 'install-bedrock' ]: () => {
    if (config('project.type') !== 'wordpress') {
      return;
    }

    return gulp.src('./vendor/roots/bedrock/web/**/*')
      .pipe(changed(config('paths.public')))
      .pipe(gulp.dest(config('paths.public')));
  },

  /**
   * Recover original wordpress files from composer cache
   * (in case that original wp files has changed during some debug)
   *
   *
   */
  ['recover-wp']: () => {
      if (config('project.type') !== 'wordpress') {
        return;
      }

      del(`${config('paths.public')}/wp/**`);
      return gulp.start('composer');
  },

  /**
   * Copy all the theme files from the framework folder. It is assumed framework folder
   * is in the same folder as the project. Task automatically replaces all occurances
   * of "themeName" with actual theme name, that must be specified with "name" parameter:
   * gulp wp:install-theme --name twentyseventeen
   */
  [ 'install-theme' ]: () => {
    let name = param('name');
    if (!name) {
      messageFromFile('env/messages/wp-theme-name-missing');
      return;
    }

    let themePath = process.cwd() + '/../WP-FRAMEWORK';
    let installPath = `${config('paths.source')}/app/themes/${name}`;

    // Make sure we don't overwrite the theme if it already exists.
    if (fileExists(installPath)) {
      console.warn('Theme with this name already exists!');
      return;
    }

    // Those files and folders will NOT be copied.
    let blacklist = [
      '.git',
      '.gitignore',
      '.idea',
      'node_modules'
    ];

    // Copy files from framework repo to the source.
    let files = fs.readdirSync(themePath);
    files.forEach(file => {
      if (blacklist.indexOf(file) > -1) {
        return;
      }

      fs.copySync(`${themePath}/${file}`, `${installPath}/${file}`);
    });

    // Replace 'themeName' with actual theme name.
    replace({
      regex: 'themeName',
      replacement: name,
      paths: [ installPath ],
      recursive: true,
      silent: true
    });
  },

  /**
   * A collection of all tasks pertaining to the database.
   */
  db: {

    /**
     * Create a DB if it doesn't exist yet.
     * wpcli automatically retrieves DB name from WP configuration.
     */
    create: () => {
      if (config('project.type') !== 'wordpress') {
        return;
      }

      wpcli('db create');
    },

    /**
     * Create an SQL file containing the database of the project.
     * The task accepts name of the target SQL file as 'file' parameter:
     * gulp wp:db:dump --file myTargetFile
     *
     * If the parameter is not provided, then the config will be used ('wordpress.dump')
     * as a default.
     *
     * The task also replaces the URL with a template tag to allow to version
     * the project and move the dump between computers.
     */
    dump: () => {
      let target = param('file');
      if (!target) {
        target = config('wordpress.dump');
      }

      dump(target);
    },

    /**
     * Import data from an SQL file.
     * The task accepts name of the target SQL file as 'file' parameter:
     * gulp wp:db:import --file myTargetFile
     *
     * If the parameter is not provided, then the config will be used ('wordpress.dump')
     * as a default.
     *
     * The task first tries to import .tpl file, replacing all template variables
     * with Site URL and Home URL from the configuration. If not available then
     * regular SQL file will be imported (no substitutions made).
     */
    import: () => {
      let target = param('file');
      if (!target) {
        target = config('wordpress.dump');
      }

      if (fileExists(`resources/dumps/${target}.sql.tpl`)) {
        serializedReplace(
          `resources/dumps/${target}.sql.tpl`,
          [
            [ '<%= siteUrl %>', process.env.WP_SITEURL ],
            [ '<%= homeUrl %>', process.env.APP_URL ]
          ],
          `resources/dumps/${target}.sql.tpl.tmp`
        );

        wpcli(`db import resources/dumps/${target}.sql.tpl.tmp`);
        fs.removeSync(`resources/dumps/${target}.sql.tpl.tmp`);

        return;
      } else if (fileExists(`resources/dumps/${target}.sql`)) {
        wpcli(`db import resources/dumps/${target}.sql`);
        return;
      }

      console.warn('SQL file doesn\'t exist.');
    },

    /**
     * Replace Site URL and Home URL in the SQL dump file with whatever is configured
     * in the .env file.
     *
     * Task accepts following parameters:
     * --file    - Name of the SQL file (in resources/dumps) to search and replace in.
     * --siteurl - (optional) Site URL to replace.
     * --homeurl - (optional) Home URL to replace.
     *
     * Site URL is the more important one so its presence is used as a flag. It siteurl
     * parameter is present then it will be used for replacement. Home URL parameter will
     * be used to replace home URL if present, but it will default to Site URL if omitted.
     *
     * If siteurl parameter is not present, then the task will attempt to look for 'siteurl',
     * and 'homeurl' in the dump, and use found values for replacement.
     *
     * Finally if the siteurl parameter is not present, and the task does not find 'siteurl'
     * in the SQL file an error message will be shown.
     */
    [ 'replace-url' ]: () => {
      let target = param('file');
      let siteurl = param('siteurl');
      let homeurl = param('home');

      // File name was not passed to the task
      if (!target) {
        messageFromFile('env/messages/wp-db-replace-file-missing');
        return;
      }

      // Specified SQL file doesn't exist
      if (!fileExists(`resources/dumps/${target}.sql`)) {
        console.warn('SQL file doesn\'t exist');
        return;
      }

      /*
       * If site URL is passed via parameter then we are not going to look it up in the file.
       * If home URL is not specified, we assume it's the same as site URL.
       */
      if (siteurl && !homeurl) {
        homeurl = siteurl;
      }

      /*
       * If URLs were not passed via parameters we'll try to find them in SQL file.
       */
      if (!siteurl || !homeurl) {
        let data = fs.readFileSync(`resources/dumps/${target}.sql`);
        let text = new StringDecoder('utf8').write(data);
        let match;

        if (!siteurl) {
          match = text.match(/'siteurl','(.+?)'/);
          if (match) {
            siteurl = match[1];
          }
        }

        if (!homeurl) {
          match = text.match(/'home','(.+?)'/);
          if (match) {
            homeurl = match[1];
          } else {
            homeurl = siteurl;
          }
        }

        // Site URL was not found in SQL file or specified via parameter.
        if (!siteurl) {
          messageFromFile('env/messages/wp-db-replace-urls-missing');
          return;
        }
      }

      /*
       * At this point we should have a URL to replace, either from parameter or
       * found in the SQL file directly.
       * We can proceed with replacing.
       */
      fs.copySync(
        `resources/dumps/${target}.sql`,
        `resources/dumps/${target}.sql.tmp`
      );

      serializedReplace(
        `resources/dumps/${target}.sql.tmp`,
        [
          [ siteurl, process.env.WP_SITEURL ],
          [ homeurl, process.env.APP_URL ]
        ],
        `resources/dumps/${target}.sql`
      );

      fs.removeSync(`resources/dumps/${target}.sql.tmp`);
    }
  }
};
