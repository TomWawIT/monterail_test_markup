let wpcli             = require('./wpcli');
let serializedReplace = require('./replaceText');

/**
 * Create an SQL export file with a given name.
 *
 * @param {string} file - Export file name, in the resources/dumps directory, without extension.
 */
module.exports = file => {
  wpcli(`db export resources/dumps/${file}.sql`);
  serializedReplace(
    `resources/dumps/${file}.sql`,
    [
      [ process.env.WP_SITEURL, '<%= siteUrl %>' ],
      [ process.env.APP_URL, '<%= homeUrl %>' ]
    ],
    `resources/dumps/${file}.sql.tpl`
  );
};
