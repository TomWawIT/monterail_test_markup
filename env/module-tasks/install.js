/*
 |--------------------------------------------------------------------------
 | Module-specific installation
 |--------------------------------------------------------------------------
 |
 | This is implemented in more specific modules, but the base version
 | of environment does not need additional installation.
 |
 */

module.exports = callback => { callback(); };
