/*
 |--------------------------------------------------------------------------
 | Module-specific watch tasks
 |--------------------------------------------------------------------------
 |
 | More specific modules might require additional watch functionality,
 | and implement this function.
 |
 */

module.exports = () => { };
