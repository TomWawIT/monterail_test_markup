let fs          = require('fs');
let path        = require('path');
let gulp        = require('gulp');
let watch       = require('gulp-watch');
let runSequence = require('run-sequence');

let config          = require('./env/lib/getProjectConfig');
let loadEnv         = require('./env/lib/loadEnv');
let fileExists      = require('./env/lib/fileExists');
let messageFromFile = require('./env/lib/messageFromFile');

/*
|--------------------------------------------------------------------------
| Verify and load configuration
|--------------------------------------------------------------------------
|
| Check if required configuration files exist.
|
*/

if (
  !fileExists('config/project.json') ||
  !fileExists('package.json')
) {
  messageFromFile('env/messages/config-missing');
  process.exit();
}

if (
  'wordpress' === config('project.type') &&
  (
    !fileExists('.env') ||
    !fileExists('composer.json')
  )
) {
  messageFromFile('env/messages/config-missing');
  process.exit();
}

loadEnv();

/*
|--------------------------------------------------------------------------
| Load tasks
|--------------------------------------------------------------------------
|
| In order to avoid having a very large and difficult to manage gulpfile
| tasks are set up and configured each in its own file, in gulp/tasks
| folder.
|
| To learn how tasks are loaded, and how task file structure is mapped
| to the task name read comments for env/gulp/taskLoader.js.
|
*/

let taskLoader = require('./env/gulp/taskLoader');
taskLoader('env/gulp/tasks');

/*
 |--------------------------------------------------------------------------
 | Define tasks
 |--------------------------------------------------------------------------
 */

/*
 * All setup required to prepare environment for development.
 */
gulp.task('install', callback => {
  require('./env/module-tasks/install')(callback);
});

/*
 * Compile everything built using preprocessors and templates.
 */
gulp.task('compile', callback => {
  runSequence(
    'html',
    'sprites',
    'styles',
    config('components.webpack') ? 'webpack' : 'scripts',
    callback
  );
});

/*
 * Check code quality and standards.
 */
gulp.task('lint', callback => {
  runSequence(
    'scripts:lint',
    callback
  );
});

/*
 * Perform a full build of the project - transform sources into full working project
 * in the public directory, and generate filepack with project files if in production.
 */
gulp.task('build', callback => {
  let buildFile = `./env/module-tasks/${config('project.type')}/build`;
  if (!fileExists(`${buildFile}.js`)) {
    return;
  }

  require(buildFile)(callback);
});

/*
 * Tasks for active development.
 * Watch for changes made during development and live update the compiled project.
 */
gulp.task('watch', () => {
  global.isWatching = true;

  let watcher = watch(`./${config('paths.source')}/**/*`);
  watcher.on('change', file => {
    let extension = path.extname(file);

    // Process HTML
    if ('.html' === extension) {
      gulp.start('html');
    }

    // Compile scripts
    else if ('.js' === extension && !config('components.webpack')) {
      gulp.start('scripts');
    }

    // Compile styles
    else if (['.scss', '.sass'].includes(extension)) {
      gulp.start('styles');
    }

    // Copy all other files
    else {
      gulp.start('copy:source');
    }
  });

  // Run webpack watch if enabled
  if (config('components.webpack')) {
    gulp.start('webpack:watch');
  }

  // Run Browsersync
  gulp.start('browsersync');

  // Module specific watch functionality
  require('./env/module-tasks/watch')();
});

/*
 * Define behavior when typing just "gulp" without task specified
 */
gulp.task('default', callback => {
  runSequence(
    'build',
    'watch',
    callback
  );
});

/*
 * This is run on the server.
 */
gulp.task('deploy', callback => {
  require('./env/module-tasks/deploy')(callback);
});
