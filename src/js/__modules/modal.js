const appModal = (function() {
	////////////////////////////////////////////////////////
	// DOM elements
	const DOMStrings = {
		// modal element:
		modal: document.getElementById('modal'),

		// elements responsible for showing modal:
		showBtn: document.querySelectorAll('.modal-open'),

		// elements responsible for hiding modal:
		hideBtn: document.querySelectorAll('.modal-close')
	};

	////////////////////////////////////////////////////////
	// Events handlers:
	DOMStrings.showBtn.forEach(function(el) {
		el.addEventListener('click', showModal);
	});

	DOMStrings.hideBtn.forEach(function(el) {
		el.addEventListener('click', hideModal);
	});


	////////////////////////////////////////////////////////
	// Functions

	////////////////////////////
	// Modal functions:
	function showModal() {
		DOMStrings.modal.classList.add('open');
		// Scroll to top
		scrollTo(document.documentElement, 0, 300);
		scrollTo(document.body, 0, 300);
	}

	function hideModal() {
		DOMStrings.modal.classList.remove('open');
	}

	////////////////////////////
	// Cross-browser javascript smooth-scroll function:
	function scrollTo(element, to, duration) {
		if (duration <= 0) return;
		var difference = to - element.scrollTop;
		var perTick = difference / duration * 10;

		setTimeout(function() {
			element.scrollTop = element.scrollTop + perTick;
			if (element.scrollTop === to) return;
			scrollTo(element, to, duration - 10);
		}, 10);
	}


	////////////////////////////////////////////////////////
	// Return 'hide' and 'show' Modal functions - to be available from the outside
	return{
		show : showModal,
		hide : hideModal
	}
})();
