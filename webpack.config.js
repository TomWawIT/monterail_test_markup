let webpack      = require('webpack');
let config       = require('./env/lib/getProjectConfig');
let isProduction = require('./env/lib/isProduction');

let settings = {
  context: `${__dirname}/${config('paths.source')}`,
  entry: './script.js',

  output: {
    path: `${__dirname}/${config('paths.public')}`,
    filename: 'bundle.js'
  },

  devtool: 'source-map',

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: [
            'babel-preset-latest',
            'babel-preset-react',
          ].map(require.resolve)
        }
      }
    ]
  }
};

if (isProduction()) {
  settings.plugins = [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    })
  ];
}

module.exports = settings;
